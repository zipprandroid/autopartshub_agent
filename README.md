## One touch zippr

App built for auto parts agents in HYD to create zippr with one touch

After login, when create zippr is clicked, your current location with desired accuracy is taken and zippr is created for the location.
 
Please take login credentials from Moyukh