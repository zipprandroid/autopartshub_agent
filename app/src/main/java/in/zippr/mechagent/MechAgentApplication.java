package in.zippr.mechagent;

import android.app.Application;

import in.zippr.mechagent.utils.ZPParseManager;

public class MechAgentApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ZPParseManager.getSharedInstance(this).initializeParse(this);
    }

    public static boolean DEBUG = false;
}
