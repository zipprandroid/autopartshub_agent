package in.zippr.mechagent;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import in.zippr.mechagent.utils.ZPConstants;
import in.zippr.mechagent.utils.ZPCreateZipprCallback;
import in.zippr.mechagent.utils.ZPLocationManager;
import in.zippr.mechagent.utils.ZPParseManager;

public class ZPCongratsFragment extends Fragment implements ZPLocationManager.LocationManagerCallbacks{

    private static final String TAG = ZPCongratsFragment.class.getSimpleName();
    private static final boolean DEBUG = MechAgentApplication.DEBUG;
    private OnCongratsFragmentListener mListener;
    private ZPLocationManager mLocationManager;
    private boolean isExpectedAccuracy = false;
    private TextView mZipprCodeTextView;
    private Button mDoneButton;
    private ProgressBar mProgressIndicator;
    private TextView mErrorText;
    private String mZipprCode;

    public static ZPCongratsFragment newInstance() {
        ZPCongratsFragment fragment = new ZPCongratsFragment();
        return fragment;
    }

    public ZPCongratsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLocationManager = ZPLocationManager.newInstance(getActivity(), this);

        if(DEBUG) Log.d(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(DEBUG) Log.d(TAG, "onCreateView");
        return inflater.inflate(R.layout.fragment_zpcongrats, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mZipprCodeTextView = (TextView) view.findViewById(R.id.text_zippr_code);
        mDoneButton = (Button) view.findViewById(R.id.done);
        mProgressIndicator = (ProgressBar) view.findViewById(R.id.progress);
        mErrorText = (TextView) view.findViewById(R.id.error_text);
        mDoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDone();
            }
        });

        if(DEBUG) Log.d(TAG, "onViewCreated");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnCongratsFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCongratsFragmentListener");
        }
        if(DEBUG) Log.d(TAG, "onAttach");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

        if(DEBUG) Log.d(TAG, "onDetach");
    }

    @Override
    public void onStart() {
        super.onStart();
        if(TextUtils.isEmpty(mZipprCode)) {
            isExpectedAccuracy = false;
            mLocationManager.startUpdates();
            showProgress(true);
            mDoneButton.setText("Getting location...");
        }else{
            showProgress(false);
            mZipprCodeTextView.setText(mZipprCode);
            mDoneButton.setText("Done");
        }

        if(DEBUG) Log.d(TAG, "onStart");
    }

    private void showProgress(boolean shouldShow) {
        mProgressIndicator.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
        if(shouldShow){
            mErrorText.setVisibility(View.GONE);
        }
        mDoneButton.setEnabled(!shouldShow);
    }

    @Override
    public void onStop() {
        super.onStop();

        mLocationManager.stopUpdates();

        if(DEBUG) Log.d(TAG, "onStop");
    }

    @Override
    public void onPause() {
        super.onPause();
        if(DEBUG) Log.d(TAG, "onPause");
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Congrats!!!");
        if(DEBUG) Log.d(TAG, "onResume");
    }

    @Override
    public void onCurrentLocationUpdated(Context context, Location newLocation) {
        if(!isExpectedAccuracy && newLocation.getAccuracy() < 40){
            if(DEBUG) Log.d(TAG, "Location received");
            mDoneButton.setText("Creating zippr...");

            mLocationManager.stopUpdates();
            isExpectedAccuracy = true;

            createZipprWithLocation(newLocation);
        }
    }

    private void createZipprWithLocation(Location newLocation) {
        JSONObject obj = new JSONObject();
        try {
            JSONObject add = new JSONObject();
            add.put("zp_v", 3);
            add.put("country", "India");
            JSONObject loc = new JSONObject();
            loc.put(ZPConstants.ServerKeys.latitude, newLocation.getLatitude());
            loc.put(ZPConstants.ServerKeys.longitude, newLocation.getLongitude());
            loc.put("__type", "GeoPoint");

            obj.put(ZPConstants.ServerKeys.location, loc);
            obj.put(ZPConstants.ServerKeys.address, add);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(getActivity() != null) {
            if(DEBUG) Log.d(TAG, "creating zippr");
            ZPParseManager.getSharedInstance(getActivity()).createZippr(getActivity(), obj, new ZPCreateZipprCallback() {
                @Override
                public void onRequestCompleted(String zipprCode, Exception e) {
                    if (e == null) {
                        mZipprCodeTextView.setText(zipprCode);
                        mZipprCode = zipprCode;
                        if(DEBUG) Log.d(TAG, "zippr created");
                    } else {
                        showError(e.getMessage());
                    }
                    showProgress(false);
                    mDoneButton.setText("Done");
                }
            });
        }else{
            // Activity went to the backgroud
        }
    }


    private void showError(String message) {
        mErrorText.setVisibility(View.VISIBLE);
        mErrorText.setText(message);
    }

    @Override
    public void onConnectionSuspended(Context context, Exception exception) {

    }

    @Override
    public void onConnectionFailed(Context context, ConnectionResult connectionResult) {
        if(connectionResult != null && !connectionResult.isSuccess() && getActivity() != null){
            Toast.makeText(getActivity(), "Please install OR update google play services.", Toast.LENGTH_LONG).show();
            getActivity().finish();
        }
    }

    public interface OnCongratsFragmentListener {
        void onDone();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(DEBUG) Log.d(TAG, "onSavedInstanceState");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(DEBUG) Log.d(TAG, "onActivityCreated " + savedInstanceState);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(DEBUG) Log.d(TAG, "onViewStateRestored");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // This view is being destroyed
        mZipprCode = null;

        if(DEBUG) Log.d(TAG, "onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(DEBUG) Log.d(TAG, "onDestroy");
    }
}
