package in.zippr.mechagent;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import in.zippr.mechagent.utils.ZPConstants;

/**
 * A placeholder fragment containing a simple view.
 */
public class ZPCreateZipprFragment extends Fragment {

    private Button mCreateZipprButton;
    private OnCreateZipprListener mListener;

    public ZPCreateZipprFragment() {
    }

    public interface OnCreateZipprListener{
        void onCreateZipprRequested();
    }

    public static ZPCreateZipprFragment newInstance(){
        ZPCreateZipprFragment f = new ZPCreateZipprFragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_zpcreate_zippr, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCreateZipprButton = (Button) view.findViewById(R.id.button_create_zippr);
        TextView userTextView = (TextView) view.findViewById(R.id.user);
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        userTextView.setText(pref.getString(ZPConstants.ServerKeys.email, ""));
        mCreateZipprButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onCreateZipprRequested();
            }
        });
     }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnCreateZipprListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCreateZipprListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Create Zippr");
    }
}
