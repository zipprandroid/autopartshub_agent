package in.zippr.mechagent.utils;

public interface ZPCreateZipprCallback {
    void onRequestCompleted(String zipprCode, Exception e);
}
