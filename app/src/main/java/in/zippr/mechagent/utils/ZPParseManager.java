package in.zippr.mechagent.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.parse.Parse;
import com.parse.ParseACL;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import in.zippr.mechagent.BuildConfig;
import in.zippr.mechagent.MechAgentApplication;
import in.zippr.mechagent.R;

public class ZPParseManager {
    private static final boolean DEBUG = MechAgentApplication.DEBUG;
    private static ZPParseManager sInstance;
    private static String APP_ID;
    private static String CLIENT_KEY;
    private static String REST_API_KEY;
    private static String ZP_API_KEY;
    private static AsyncHttpClient sClient = new AsyncHttpClient();

    private static final String CONTENT_TYPE = "application/json";
    private static final String CREATE_ZIPPR_CLOUD_CODE_URL = "https://api.parse.com/1/functions/createZippr_v3";

    static public synchronized ZPParseManager getSharedInstance(Context context) {
        if (sInstance == null) {
            sInstance = new ZPParseManager();

            APP_ID = context.getString(DEBUG ? R.string.appid : R.string.appid_prod);
            CLIENT_KEY = context.getString(DEBUG ? R.string.client_key : R.string.client_key_prod);
            REST_API_KEY = context.getString(DEBUG ? R.string.restapi_key : R.string.restapi_key_prod);
            ZP_API_KEY = context.getString(R.string.zp_api_key);

            sInstance.initializeParse(context);
            sInstance.addParseHeaders();
            sClient.setTimeout(8000);
        }
        return sInstance;
    }

    public void
    initializeParse(Context context) {
        Parse.initialize(context, APP_ID, CLIENT_KEY);
        setDefaultAccessPermissions();
    }

    private void setDefaultAccessPermissions() {
        ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(false);
        ParseACL.setDefaultACL(defaultACL, true);
    }

    private void addParseHeaders() {
        sClient.addHeader("X-Parse-Application-Id", APP_ID);
        sClient.addHeader("X-Parse-REST-API-Key", REST_API_KEY);
        sClient.addHeader("Content-Type", CONTENT_TYPE);
    }

    public void createZippr(Context context, JSONObject jsonObject, final ZPCreateZipprCallback cb){
        StringEntity entity = null;
        try {
            jsonObject.put(ZPConstants.ServerKeys.zpapikey, ZP_API_KEY);

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            String userObjId = prefs.getString(ZPConstants.ServerKeys.userObjId, "");

            JSONObject user = new JSONObject();
            user.put("__type", "Pointer");
            user.put("className", "_User");
            user.put("objectId", userObjId);

            jsonObject.put(ZPConstants.ServerKeys.user, user);

            entity = new StringEntity(jsonObject.toString(), "UTF-8");

        } catch (JSONException e) {
            cb.onRequestCompleted(null, e);
        } catch (UnsupportedEncodingException e) {
            cb.onRequestCompleted(null, e);
        }
        JsonHttpResponseHandler respHandler = new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    String zipprCode = response.getJSONObject("result").getString("zippr");
                    cb.onRequestCompleted(zipprCode, null);
                } catch (JSONException e) {
                    cb.onRequestCompleted(null, e);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                cb.onRequestCompleted(null, new Exception(throwable));
            }
        };

        sClient.post(context, CREATE_ZIPPR_CLOUD_CODE_URL, entity, CONTENT_TYPE, respHandler);
    }
}
