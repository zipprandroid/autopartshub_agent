package in.zippr.mechagent.utils;

import android.content.Context;
import android.content.Intent;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import in.zippr.mechagent.BuildConfig;

/**
 * Handles everything related to location updates(starting/stopping/failed connections), implement
 * {@link in.zippr.mechagent.utils.ZPLocationManager.LocationManagerCallbacks} to get the callbacks.
 */
public final class ZPLocationManager implements
		GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
		LocationListener {

	private static final String TAG = ZPLocationManager.class.getSimpleName();
	private static final boolean DEBUG = BuildConfig.DEBUG;

	/**
	 * The desired interval for location updates. Inexact. Updates may be more or less frequent.
	 */
	private static int UPDATE_INTERVAL_MIN_TIME = 0; // In millisec
	/**
	 * The fastest rate for active location updates. Exact. Updates will never be more frequent
	 * than this value.
	 */
	private static int FASTEST_UPDATE_INTERVAL = 0; // In millisec
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	private static ZPLocationManager sInstance = null;
	private final LocationManager mLocationManager;

	private Context mContext;
	/**
	 * Provides the entry point to Google Play services.
	 */
	private final GoogleApiClient mGoogleApiClient;
	/**
	 * Stores parameters for requests to the FusedLocationProviderApi.
	 */
	private final LocationRequest mLocationRequest;
	/**
	 * The last known location
	 */
	private Location mLastLocation;

	private boolean isLocationUpdatesOn;

	/**
	 * Callbacks for communication with {@code ZPLocationManager}.
	 */
	public interface LocationManagerCallbacks{
		/**
		 * Called when the location is changed
		 * @param newLocation the new location
		 */
		void onCurrentLocationUpdated(Context context, Location newLocation);
		/**
		 * Called when the connection to the location client is suspended due to some error
		 * @param exception get the suspension error with {@link Exception#getMessage()}
		 */
		void onConnectionSuspended(Context context, Exception exception);
		/**
		 * {@link GoogleApiClient.OnConnectionFailedListener#onConnectionFailed(ConnectionResult)}
		 */
		void onConnectionFailed(Context context, ConnectionResult connectionResult);
	}

	public interface LocationServiceStatusCallback{
		void onStatusChanged(boolean isConnected);
	}

	private LocationManagerCallbacks mLocationManagerCallbacks;
	private LocationServiceStatusCallback mLocationServiceStatusCallback;

	private ZPLocationManager(Context context) {
		mContext = context;
		mGoogleApiClient = new GoogleApiClient.Builder(context)
				.addApi(LocationServices.API)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.build();
		mLocationRequest = LocationRequest.create();
		mLocationRequest.setInterval(UPDATE_INTERVAL_MIN_TIME);
		mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		mLocationManager.addGpsStatusListener(new GpsStatus.Listener() {
			@Override
			public void onGpsStatusChanged(int event) {
				if (mLocationServiceStatusCallback == null)
					return;
				if (!isLocationUpdatesOn)
					return;
				if (event == GpsStatus.GPS_EVENT_STARTED) {
					mLocationServiceStatusCallback.onStatusChanged(true);
				} else if (event == GpsStatus.GPS_EVENT_STOPPED) {
					mLocationServiceStatusCallback.onStatusChanged(false);
				}
			}
		});
	}

	@Deprecated
	public static synchronized ZPLocationManager getSharedInstance(Context context) {
		if (sInstance == null) {
			sInstance = new ZPLocationManager(context);
		}
		return sInstance;
	}

	public static ZPLocationManager newInstance(Context context, LocationManagerCallbacks cb) {
		ZPLocationManager inst = new ZPLocationManager(context);
		inst.mLocationManagerCallbacks = cb;
		return inst;
	}

	/**
	 * Set the callbacks for requester
	 */
	public void setLocationManagerCallbacks(LocationManagerCallbacks callbacks){
		mLocationManagerCallbacks = callbacks;
	}

	public void setLocationServiceStatusCallback(LocationServiceStatusCallback callbacks){
		mLocationServiceStatusCallback = callbacks;
	}

	/**
	 * Starts the location updates. <p>Add {@link LocationManagerCallbacks}
	 * to listen to location updates.</p>
	 * <p>Call this method when the UI is visible</p>
	 */
	public void startUpdates() {
		if(isConnected()) {
			startLocationUpdates();
		} else {
			mGoogleApiClient.connect();
		}
	}

	/**
	 * Stop the location updates. <p>Call this when your UI is off the screen</p>
	 */
	public void stopUpdates() {
		stopLocationUpdates();
	}

	/**
	 * Get the most recent known location
	 * @return a valid {@link Location} if location services are enabled, a {@link Location}
	 * initialised to lat,long=0 otherwise
	 */
	@Nullable
	public Location getLastKnownLocation() {
		return mLastLocation;
	}

	/**
	 * @return {@code true} if location source is available, {@code false} otherwise
 	 */
	public boolean isLocationSourceAvailable() {
		LocationAvailability la = LocationServices.FusedLocationApi.getLocationAvailability(mGoogleApiClient);
		return la != null && la.isLocationAvailable();
	}

	/**
	 * @return {@code true} if GPS is enabled, false otherwise
	 */
	public boolean isGPSEnabled() {
		return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}

	/**
	 * @return true if network location provider is enabled or false otherwise
	 */
	public boolean isAGPSEnabled() {
		return mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
	}

	@Override
	public void onConnected(Bundle bundle) {
		mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		if (mLastLocation != null) {
			onLocationChanged(mLastLocation);
		}
		startLocationUpdates();
	}

	@Override
	public void onConnectionSuspended(int i) {
		if(DEBUG) Log.d(TAG, "onConnectionSuspended(): " + i);
		Exception e = new Exception("Error");
		if(i == GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST)
			e = new Exception("Network lost");
		else if(i == GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED)
			e = new Exception("Location services disconnected");
		if (mLocationManagerCallbacks != null)
			mLocationManagerCallbacks.onConnectionSuspended(mContext, e);
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		if(DEBUG) Log.d(TAG, "onConnectionFailed(): " + connectionResult);
		if (mLocationManagerCallbacks != null)
			mLocationManagerCallbacks.onConnectionFailed(mContext, connectionResult);
	}

	@Override
	public void onLocationChanged(Location location) {
		mLastLocation = location;
		if (mLocationManagerCallbacks != null)
			mLocationManagerCallbacks.onCurrentLocationUpdated(mContext, location);
		notifyLocationUpdate(mLastLocation);
	}

	private void startLocationUpdates() {
		PendingResult<Status> result = LocationServices.FusedLocationApi.requestLocationUpdates(
					mGoogleApiClient, mLocationRequest, this);
		isLocationUpdatesOn = true;
	}

	private void stopLocationUpdates() {
		if (isConnected()) {
			PendingResult<Status> result = LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
			isLocationUpdatesOn = false;
		}
	}

	/*
     * ! Notifies all registered observers about location change
     */
	protected void notifyLocationUpdate(Location location) {
		Intent i = new Intent(ZPConstants.LocalBroadcast.locationUpdated);
		i.putExtra(ZPConstants.BundleKeys.location, location);
		LocalBroadcastManager.getInstance(mContext).sendBroadcast(i);

		//ZPParseManager.getSharedInstance().updateInstallationWithLocation();
	}

	/**
	 * Disconnect from location services
	 */
	public void disconnectLocationService() {
		mGoogleApiClient.disconnect();
	}

	/**
	 * @return {@code true} if the client is connected to the location service
	 */
	public boolean isConnected() {
		return mGoogleApiClient.isConnected();
	}

	/**
	 * @return {@code true} if the client is attempting to connect to the service.
	 */
	public boolean isConnecting() {
		return mGoogleApiClient.isConnecting();
	}
}
