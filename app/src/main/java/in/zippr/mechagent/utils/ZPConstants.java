/*
 * Author: Srikanth Sombhatla
 * August 2013, zip.pr
 */

/*!
 * This class holds all constants used across the app
 */

package in.zippr.mechagent.utils;

import com.google.android.gms.maps.GoogleMap;

public final class ZPConstants {

	// Any zippr type less than this is owned by user.
    // Used in zp_type only.
	public final static int userOwnedTypeRange						= 200;
	public final static int TypeMeetMeHere 							= 1; 	
	public final static int TypePermanentZippr						= 2; 
	public final static int TypeBusiness							= 3; 		
	public final static int TypeEmergency                           = 5;
    public final static int TypeBuilding                            = 6;
	
	// Sidemenu options
	public final static int INVALID									=  -1;
	
	/** This order should exactly reflect the order of sidemenu items **/
	/*
	 * Each page is given an id, which is used in a command to launch a particular page.
	 * New pages should be added at the end. 
	 * If a page is removed from app, its id should not be reused since its been already exposed externally to 
	 * services like push notifications etc.
	 */
	
	/* Ordered as it appears in side menu */
    // TODO: Move these page actions into class Actions
    // These are used in Push Notifications, so do not change the values.
    // If these values are supposed to be changed, move them to Actions but retain the same values.
	public final static int PROFILE									=	0;
	public final static int INVITE_FRIENDS							=	1;
    public final static int FAQS        							=	2;
	public final static int ZIPPR_SUPPORT							=	3;
	public final static int FEEDBACK								=	4;
    /** Side menu items order end **/
	/* Ordered as it appears in side menu */

    public final static int HOME                                    =   7;

	
	public final static int SEARCH									=	50;
	public final static int AUTO_LOGOUT								=	51; // logs out without user confirmation.
    public final static int VERIFY_EMAIL                            =   52; // Show verify email dialog.
    public final static int BECOME_REG_USER                         =   53; // Asks anonymous user to become registered user
    public final static int CREATE_ZIPPR                            =   54;

    // End of actions

	public final static int MAX_ALLOWED_NEW_REG						= 	5;
	public final static int ZIPPR_CODE_LENGTH						=	8;
	public final static float PREFERRED_ZOOM						=	17;
    public final static int PREFERRED_MAP_TYPE                      =   GoogleMap.MAP_TYPE_NORMAL;
    public final static String UNKNOWN_COUNTRY_CODE                 =   "XX";

    // Pictures
    public final static int PREF_PIC_WIDTH                          =   1290;
    public final static int PREF_PIC_HEIGHT                         =   720;

    // Limits
    public static final int MAX_NOTIFICATIONS                       =   100;
    public static final int MAX_NEARBY_ZIPPRS                       =   20;

	public static final double DEFAULT_ADD_BUILDING_SEARCH_RADIUS   =   0.5;

	// All types
	public enum Types {
		Invalid,
		// Zippr types
		MyZippr,
		RecentZippr,
		FavoriteZippr
	}
	
	public final static String BUGSENSE_APIKEY					=	"07e9b9cc";
	public final static String ZP_URI_SCHEME					=	"zippr";
	public final static String ZP_ANDROID_REF_CODE				=	"100100100";
    public static final String DATABASE_NAME                    =   "zipprDB";
	public static final int DATABASE_VERSION 					= 	2;
	/*!
	 * Actions on zippr card.
	 */
	public static final class Actions {
		public static final int actionInvalid 			= 0;
		
		// Zippr card actions
		public static final int actionFavorite			= 1;	
		public static final int actionUnfavorite		= 2;
		public static final int actionSMS				= 3;
		public static final int actionSystemShare       = 4;
		public static final int actionCall				= 5;
		public static final int actionEdit				= 6;
		public static final int actionDelete			= 7;
		public static final int actionPointOnMap		= 8;
		public static final int actionDirections		= 9;
		public static final int actionAddToRecents		= 10;
		public static final int actionRemoveFromRecents	= 11;
        public static final int actionDetails       	= 12;
        public static final int actionToggleFavorite    = 13;
        public static final int actionCopy              = 14;
        public static final int actionDetailMapTap      = 15;
        public static final int actionQuickShare        = 16;
        public static final int actionQuickNavigation   = 17;
        public static final int actionQuickCopy         = 18;
        public static final int actionQuickEdit         = 19;
        public static final int actionPremiumRequest    = 20;


		// User actions
		public static final int actionSignup			= 200;
		public static final int actionLogin				= 201;
		public static final int actionLogout			= 202;
		public static final int actionUserUpdated		= 203;
		public static final int actionForgotPswd		= 204;
        public static final int actionSkip              = 205;
		
		// Misc
		public static final int actionGooglePlayServicesUnavailable		= 400;
		public static final int actionCreateQuickZippr	= 401;
		public static final int actionCreatePermZippr	= 402;
		public static final int actionCreateZipprFailed = 403;
		public static final int actionZipprRecieved		= 404;
		
		// User changed map position while creating zippr.
		public static final int actionMapPosChanges		= 405;
		public static final int actionZipprExtraInfo	= 406;
		
		public static final String actionStringFav      = "ZP_FAV";
		public static final String actionStringUnFav    = "ZP_UNFAV";

        // Anonymous User
        public static final int actionAnonymSignUp      = 501;
        public static final int actionAnonymLogIn       = 502;
        public static final int askLocation             = 503;

        /**
         * Used when showing the details page for the zippr with options to add picture
         */
        public static final int actionDetailsAddPicture = 550;
        public static final int actionDetailsShowPicture = 551;
        public static final int actionPicAdded = 552;
        public static final int actionPicUpdated = 553;
        public static final int actionPicFromGallery = 554;
        public static final int actionPicFromCamera = 555;

		/**
		 * Building zippr
		 */
		public static final int actionBuildingZipprOpened = 600;
		public static final int actionBuildingsOpenedNearby = 601;
		public static final int actionBuildingsPickedNearby = 602;
		public static final int actionBuildingZipprSearchWithCode = 603;
		public static final int actionBuildingZipprCreated = 604;
		public static final int actionBuildingZipprAddedToReceived = 605;

		/**
		 * In-App Share
		 */
		public static final int actionOpenedShareViaZippr            =   650;
		public static final int actionOpenedShareViaZipprAnonymous   =   651;
		public static final int actionOpenedShareViaZipprUnverified  =   652;
		public static final int actionSharedViaZippr                 =   653;
		public static final int actionSharedUsingWhatsapp            =   654;
		public static final int actionSharedUsingFb                  =   655;
		public static final int actionSharedUsingSms                 =   656;
		public static final int actionSharedUsingEmail               =   657;
		public static final int actionSharedUsingSystemShare         =   658;

		// Congrats Screen Building Zippr
		public static final int actionCGAddPic                       =   700;
		public static final int actionCGRequestPremiumZippr          =   701;

		// On-Demand address update
		public static final int actionOnDemandAddressUpdate			 =   750;
    }

    public static final class Trigger {
        // Triggers are conditions where we are showing alert to user to signup/login/verify
        // They dont have to be associated with a Zippr.
        public static final int zipprCountAnonym         = 601;
        public static final int zipprCountUnverified     = 602;
        public static final int zipprCustomAnonym        = 603;
        public static final int zipprCustomUnverified    = 604;
        public static final int login                    = 605;
        public static final int receivedParsePN          = 606;
        public static final int openedParsePN            = 607;
    }

	public static final class BundleKeys {
		public static final String latitude 					= 	"latitude";
		public static final String longitude 					= 	"longitude";
		public static final String title						=	"title";
		public static final String subTitle						=	"subtitle";
		public static final String confirmText					=	"confirmText";
        public static final String zipprModel					=	"zipprModel";
		public static final String zipprCode					=	"zipprCode";
		public static final String address						= 	"address";
		public static final String phoneNum						= 	"phonenum";
		public static final String countryCode					=	"countrycode";
		public static final String result						=	"result";
		public static final String firstName					=	"first_name";
		public static final String email						=	"email";
		public static final String userName                     =	"username";
        public static final String password						=	"password";
		public static final String primary_phone_prefix			= 	"primary_phone_prefix";
		public static final String secondary_phone_prefix		=	"secondary_phone_prefix";
		public static final String secondaryInfoPresent			=	"secondaryInfoPresent";
		public static final String there 						= 	"there";
		public static final String notThere						=	"notThere";
        public static final String height   					=	"height";
        public static final String launchMode                   =   "launchmode";
        public static final String presentationTitle            =   "presentationtite";
		public static final String uploadModel                  =   "uploadModel";

		// Location update
		public static final String location						=	"location";
		public static final String updateWithCurrentLocation 	=   "updateWithCurrentLocation";
		
		public static final String justLoggedIn				=  "justLoggedIn";
		public static final String justSignedUp				=  "justSignedUp";
		
		// Country selection
		public static final String selectedCountryName		=	"selcountryname";

        // Login Transaction
        public static final String loginAction              =  "Action.Login";
        public static final String intentHandled            =   "intenthandled";
        public static final String showCongratsMsg          =   "showcongratsmsg";

        public static final String ignoreList               =   "ignoreList";
        public static final String action                   =   "action";
        public static final String skipToLast               =   "skiptolast";

        // Page to open
        public static final String pageToOpen               =   "pagetoopen";
        public static String uploadResumedToast             =   "resume_upload";
        public static String imageViewWidth                 =   "imageview_width";
        public static String imageViewHeight                =   "imageview_height";
        public static String isResumedUpload                =   "is_resumed_upload";

		public static String serviceIntegrationModel        =   "serviceIntegrationModel";
    }
	
	public static final class ServerKeys {
		// JSON Object keys
		// These keys should match the field names documented in Server API
		// spec.
		public static final String result 					= 	"result";
		public static final String objectId 				= 	"objectId";
		public static final String zippr 					= 	"zippr";
		public static final String location 				= 	"location";
		public static final String latitude 				= 	"latitude";
		public static final String lat                      =   "lat";
		public static final String longitude 				= 	"longitude";
		public static final String lon                      =   "lon";
		public static final String address 					= 	"address";
		public static final String address_v3				= 	"address_v3";
		public static final String countrycode 				= 	"countrycode";
		public static final String title 					= 	"title";
		public static final String type 					= 	"zp_type";
		public static final String zpapikey					= 	"zp_api_key";
		public static final String primaryPhone				= 	"primary_phone";
		public static final String secondaryPhone			= 	"secondary_phone";
		public static final String recoveryEmail			=	"recovery_email";
		public static final String zpURL					=	"zp_url";
		public static final String timeStamp				=	"timeStamp";
		public static final String password					=	"password";
		public static final String userName					=	"username";
		public static final String email					=	"email";
        public static final String emailVerified            =   "email_verified";
		public static final String firstName				=	"first_name";
		public static final String lastName					=	"last_name";
		public static final String refCode					=	"ref_code";
		public static final String refPoints				=	"ref_points";
		public static final String shareId					=	"share_id";
		public static final String userToken				=	"user_token";
		public static final String user						=	"user";
		public static final String feedback					=	"feedback";
		public static final String voice					=	"voice";
		public static final String primary_phone_prefix		= 	"primary_phone_prefix";
		public static final String bussPhoneNums			=	"international_phone_number";
        public static final String assumed_type             =   "assumed_type";
        public static final String version                  =   "version";
        public static final String userType                 =   "user_type";
		public static final String createdBy				=	"created_by";

		// These keys should be ignored while updating to parse
		public static final String updatedAt				= 	"updatedAt";
		public static final String createdAt				= 	"createdAt";
		//
		
		// Search and Explore
		public static final String category					=	"category";
		public static final String query					=	"query";
		public static final String zpCategory				=	"zp_category"; // category key in extra_info
		
		// Address components
		public static final String userLocality				= 	"user_locality";
		public static final String userBuildingName			= 	"user_buildingname";
		public static final String userAptNum				= 	"user_aptnum";
		public static final String country   				=	"country";
		public static final String formattedAddress			=	"formatted_address";
		public static final String phone				   	=	"phone";

		// Premium zippr request
		public static final String given				   	=	"given";
		public static final String desired				   	=	"desired";
		public static final String comments				   	=	"comments";
		
		// Transactions
		public static final String src 						=	"src";
		public static final String userObjId 				=	"user_objectId";
        public static final String anonymOwnerObjId         =   "owner_user_obj_id";
        public static final String anonyObjId               =   "anon_user_obj_id";
        public static final String anonyZipprs              =   "zippr_codes";
		public static final String action 					=	"action";
		public static final String timestamp				=	"timestamp";
		public static final String clientInfo				=	"client_info";
		public static final String actionData				=	"action_data";
		public static final String gps						=	"gps";
		public static final String agps						=	"agps";
		public static final String duration					=	"duration";
		
        public static final String extraInfo                =   "extra_info";
        // Map info params
        public static final String mapInfo                  =   "map_info";
        public static final String zoom                     =   "zoom";
        public static final String mapType                  =   "map_type";
        public static final String accuracy                 =   "accuracy";
		public static final String mapSrc                   =   "map_src";

        public static final String android                  =   "android";
        public static final String ios                      =   "ios";

		public static final String mapbox                   =   "mapbox";
		// Installations

        public static final String zipprCodes              =   "zippr_codes";

        // Subscriptions
        public static final String favorites                =   "favorites";
        public static final String recents                  =   "recents";
        public static final String subscriptions_meta_info  =   "subscriptions_meta_info";
        public static final String operation                =   "operation";
        public static final String append                   =   "append";
        public static final String remove                   =   "remove";

        // Contacts
        public static final String contactDisplayName       =   "displayName";
        public static final String contactPhotoThumbnailUri =   "photoThumbnailUri";
        public static final String contactPhoneNumber       =   "phoneNumber";
    //    public static final String contactPhoneNumbers      =   "phoneNumbers";
        public static final String contactEmail             =   "email";
    //    public static final String contactEmails            =   "emails";
        public static final String contactLookupKey         =   "lookup_key";

        // Pictures
        public static final String zipprMedia               =   "zippr_media";
        public static final String images                   =   "images";
        public static final String defaultImage             =   "default";
        public static final String thumbnailImage           =   "thumbnail";
        public static final String url                      =   "url";
        public static final String description              =   "description";
        public static final String visibility               =   "visibility";
        public static final String size                     =   "size";

        // Notifications
        public static final String notificationReceivers      = "notification_receivers";
        public static final String notificationData           = "notification_data";
        public static final String notificationStatus         = "notification_status";
        public static final String notificationStatusKey      = "status";
        public static final String notificationExpiry         = "notification_expiry";

        // Notification Server Headers
        public static final String apiKey               =   "x-zippr-api-key";
        public static final String contentType          =   "Content-Type";

        // Notification Status Values
        public static final String received             =   "received";
        public static final String acted                =   "acted";

        // In-App Share
        public static final String contactFirstName     =   "contact_first_name";
        public static final String contactLastName      =   "contact_last_name";
        public static final String contactPhoneNumbers  =   "contact_phone_numbers";
        public static final String contactEmails        =   "contact_emails";

        // WhereAmI
        public static final String ok                   =   "ok";
        public static final String city                 =   "city";

		// Reverse Geocoding
		public static final String addresses            =   "addresses";

		// Geocoding
		public static final String searchString                  =   "search_string";
		public static final String formattedPrimaryString        =   "formatted_primary_string";
		public static final String formattedSecondaryString      =   "formatted_secondary_string";
		public static final String recommendations               =   "recommendations";
		public static final String displayString                 =   "display_string";
		public static final String zoomLevel                     =   "zoom_level";
		public static final String matchedAddressField           =   "matched_address_field";
		public static final String searchToken                   =   "search_token";

		// Service Integration
		public static final String clientId             =   "client_id";
		public static final String displayName          =   "display_name";
		public static final String serviceDescription   =   "description";
		public static final String callToAction         =   "call_to_action";
		public static final String thumbImg             =   "thumb_img";
		public static final String regularImg           =   "regular_img";
		public static final String offerImg             =   "offer_img";
		public static final String successUrl           =   "success_url";
		public static final String serviceAvailable     =   "service_available";
		public static final String orderUrl             =   "order_url";
    }

	// Error keys
	public static final class ErrorKeys {
		public static final String error					=	"error";
		public static final String ok						=	"ok";
		public static final String code						=	"code";
	}
	
	// Internal model keys. Should not be synced to server.
	public static final class ModelKeys {
		public static final String userObjectId			   	=	"__zpUserObjectId";
        public static final String picture                  =   "__picture";
        public static final String isFavorite               =   "__isFavorite";
        public static String internalZipprType              =   "__internalType";
    }
	
	public static final class Parse {
		// Parse cloud functions
		public static final String createZippr 									  = 	  "createZippr_v3";
		public static final String upgradeZipprsToV3							  = 	  "convertZipprsToV3";
		public static final String geocode 									      = 	  "geocode";
		public static final String search										  = 	  "search";
		public static final String reverseGeocode 								  = 	  "reverseGeocode";
		public static final String registerPhone 								  = 	  "registerWithPhone";
		public static final String verifyPhone   								  = 	  "verifyRegisterWithPhone";
		public static final String sendResetPswdPintoEmail                        =       "sendResetPasswordPinToEmail";
        public static final String sendResetPswdPintoSecndaryphone                =       "sendResetPasswordPinToSecondaryPhone";
        public static final String verifyResetPassword                            =       "verifyResetPassword";
		public static final String createZipprWithLocation						  = 	  "createZipprWithLocation";
		public static final String resetPasswordWithVerificationPin				  =		  "resetPasswordWithVerificationPin";
		public static final String sendResetPasswordLinkToEmail					  =		  "sendResetPasswordLinkToEmail";
        public static final String anonymousVerifyPhone                           =       "verifyPhone";
        public static final String anonymousRegister                              =       "anonToRegistered";
		@Deprecated
        public static final String anonymousZipprTransfer                         =       "changeOwnerOfZipprs";
        public static final String signupAnonymous                                =       "signupAnonymous";
        public static final String verificationEmail                              =       "sendVerificationEmail";
        public static final String syncSubscriptions                              =       "syncSubscriptions";

		// Parse class names
		public static final String classNameZipprs								  =		  "Zipprs";
		public static final String classNameFeedback							  =		  "Feedback";
		public static final String classNamePremiumRequest						  =		  "PremiumRequest";
		public static final String classNameTransactions						  =		  "Transactions";
        public static final String classNameConfigurations                        =       "Configurations";
        public static final String classNameNotifications                         =       "ZipprNotifications";
        public static final String classNameImageDownloadTransactions             =       "PictureMetrics";
	}
	
	// Preferences keys
	public static final class PrefKeys {
		public static final String swipeTutorialShown   	=		"swipetutorialShown";
		public static final String newRegCount				=		"newRegCount";
		public static final String countryCodesFilled		=		"countrycodesfilled";
        public static final String mapType      			=		"maptype";
        public static final String firstTime                =       "key.firstTime";
        public static final String canShowShareTip          =       "com.zippr.canshowsharetip";
        public static final String canShowAskLocTip         =       "com.zippr.canshowaskloctip";
        public static final String newUserAddLocDialogShown =       "com.zippr.newuseraddlocdlgshown";
        public static final String selectedSegmentPos       =       "com.zippr.selectedsegpos";
        public static final String subscriptionSynced       =       "com.zippr.subscriptionsynced";
        public static final String syncSubscriptonImmediately =     "com.zippr.syncsubscriptonimmediately";
        public static final String lastNotificationSync     =       "lastNotificationSync";
        public static final String shouldRefreshImmediately =       "com.zippr.shouldrefreshnimmediately";
        public static final String whereAmIResult           =       "com.zippr.whereamiresult";
		public static final String isLoggedInUserUpdatedInstallation = "updateInstallation";
		public static final String isRegisteredForPushNotifications  = "registerForPushNotifications";

		// Debug keys
		public static final String debugAlwaysFindUserCity	=		"com.zippr.debug.alwaysfindusercity";
	}

	// Local broadcast actions
	public static final class LocalBroadcast {
        public static final String anonymousUser            =       "com.zippr.anonuser";
        public static final String signup					=		"con.zippr.signup";
		public static final String login					=		"con.zippr.login";
		public static final String logout					=		"com.zippr.logout";
		public static final String locationUpdated			=		"com.zippr.locationupated";
        // Called when global layout is completed on layout tree.
        // Use this broadcast to calculate view heights
        public static final String layoutCompleted			=		"com.zippr.layoutcompleted";

        // Notifications
        // When new notification is added, removed or acted on.
        public static final String notificationsUpdated     =       "com.zippr.notifupdated";

        // Server configuration fetched/failed.
        public static final String configFetched            =       "com.zippr.configfetched";
        public static final String configFetchFailed 		=       "com.zippr.configfetchedfailed";

		// Building zippr
		/**
		 * Sent when add building zippr flow is finished.
		 */
		public static final String finishedAddBuildingZipprFlow	=	"com.zippr.finishedbuildingzipprflow";
	}

	// Push Notifications
	public static final class PushNotification {
		public static final String payload		=	"com.parse.Data";
		public static final String channel		=	"com.parse.Channel";
		public static final String alert		=	"alert";
		public static final String dialog		=	"d";
        public static final String type 		=	"t";
        public static final String objectId     =   "o";
		
		// actions
		public static final String action				=	"a";
		public static final String actionOpenPage		=	"p";
		public static final String actionFetchZippr		=	"fz";
		public static final String actionOpenZippr		=	"oz";

        // type values
        public static final int typePremiumZippr        =    1;
        public static final int typeRequestZippr        =    2;
	}

	// Error codes contract with Server
	public static final class ErrorCodes {
		public static final int invalidRefCode				=		2127;
		public static final int phNumAlreadyInUse			=		2117;
		public static final int invalidPhNum				=		2121;
		public static final int emailAlreadyTaken			=		2131;

		// Parse
		public static final int userNameAlreadyTaken        =       202;
		public static final int noError                     =       0;
	}

    public static final class Address {
        // Rev geo response keys
        public static final String status           =   "status";
        public static final String results          =   "results";
        public static final String addressComps     =   "address_components";
        public static final String shortName        =   "short_name";
        public static final String longName         =   "long_name";
        public static final String types            =   "types";

        public static final String establishment    =   "establishment";
        public static final String parking          =   "parking";
		public static final String street           =   "street_address";
        public static final String street_number    =   "street_number";
        public static final String route            =   "route";
        public static final String adminLevel2      =   "administrative_area_level_2";
        public static final String adminLevel1      =   "administrative_area_level_1";
        public static final String countryCode      =   "countrycode";
        public static final String formattedAddress =   "formatted_address";
        public static final String location_type    =   "location_type";

        // Zippr defined
        public static final String stateAndCountry  =   "state_country"; // district, state and country

		public static final String success			=	"success";
		public static final String payload			= 	"payload";

		// V3
        // User filled
        public static final String houseNum         =   ServerKeys.userAptNum;
		public static final String unitNum          =   "unit_num";
        public static final String buildingName     =   ServerKeys.userBuildingName;
		public static final String roadName         =   "road_name";
        public static final String landmark         =   "landmark";

		public static final String subLocality      =   "sublocality";
		public static final String locality         =   "locality";
		public static final String city             =   "city";
		public static final String postalCode       =   "postal_code";
		public static final String state            =   "state";
		public static final String country          =   "country";

		public static final String version          =   "zp_v";
    }

    /*!
    Defines types of launch modes for activities
     */
    public static final class LaunchModes {
        public static final String create           =   "create";
        public static final String edit             =   "edit";
    }

    /*!
    Assumed types: Assuming a type from zippr title.
    */
    public static final class AssumedType {
        public static final String home             =   "Home";
        public static final String work             =   "Work";
        public static final String other            =   "Other";
    }

    // Activity names constants for ActivityHelper Class
    public static final class Activity {
        public static final String HOME         = "Activity.Home";
    }

    /*!
    Generic operations
     */
    public enum Operation {
        add,
        remove,
        insert,
        replace
    }

    public static final class PictureUploadLocalDBKeys {
        public static final String zippr            =   "zippr";
        public static final String json_str         =   "json_str";
        public static final String createdAt        =   "createdAt";
        public static final String status           =   "status";
        public static final String uploadId         =   "upload_id";
        public static final String absPath          =   "abs_path";
    }

	public static final class Values {
		public static final String unknownCity 		=	"Other";
	}

    public static final String ZP_API_KEY = "7077a87616779beedb57c6b231ebd906ce78c2178887c48e74905e6d327417f9";
}
