package in.zippr.mechagent;

import android.content.SharedPreferences;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.parse.ParseUser;

import in.zippr.mechagent.utils.ZPUtils;

public class ZPCreateZipprActivity extends AppCompatActivity implements
        ZPCreateZipprFragment.OnCreateZipprListener, ZPCongratsFragment.OnCongratsFragmentListener{

    private ZPCreateZipprFragment mCreateZipprFragment;
    private ZPCongratsFragment mCongratsZipprFragment;

    private static final String CREATE_ZIPPR_FRAGMENT = "create_zippr_fragment";
    private static final String CONGRATS_FRAGMENT = "congrats_fragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zpcreate_zippr);

        getSupportActionBar().setLogo(R.mipmap.orange_logo);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        if(savedInstanceState == null) {
            mCreateZipprFragment = ZPCreateZipprFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, mCreateZipprFragment, CREATE_ZIPPR_FRAGMENT)
                    .addToBackStack(null)
                    .commit();
        }else{
            mCongratsZipprFragment = (ZPCongratsFragment)getSupportFragmentManager().findFragmentByTag(CONGRATS_FRAGMENT);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_zpcreate_zippr, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
//            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
//            SharedPreferences.Editor editor = pref.edit();
//            editor.clear();
//            editor.commit();
//
//            ParseUser.logOut();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDone() {
        if(mCongratsZipprFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(mCongratsZipprFragment).commit();
            getSupportActionBar().setTitle("Create Zippr");
        }
    }

    @Override
    public void onCreateZipprRequested() {
        if(!ZPUtils.isLocationServicesEnabled(this)){
            Toast.makeText(this, "Please enable location services with high accuracy mode.", Toast.LENGTH_LONG).show();
            return;
        }
        if(mCongratsZipprFragment == null || (mCongratsZipprFragment != null && !mCongratsZipprFragment.isAdded()))
            mCongratsZipprFragment = ZPCongratsFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, mCongratsZipprFragment, CONGRATS_FRAGMENT)
                    .commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(getSupportFragmentManager().getBackStackEntryCount() == 0)
            finish();
    }
}
